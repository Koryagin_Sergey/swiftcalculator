//
//  ViewController.swift
//  Calculator
//
//  Created by Admin on 09.02.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var lblDisplay: UILabel!
    
    enum Actions {
        case unowned
        case plus
        case minus
        case divide
        case mult
    }
    
    
    
    
    var type: Actions = .unowned
    
    var firstValue: String = ""
    var secondValue: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnActions(_ sender: UIButton) {
        guard let value = sender.titleLabel?.text, let result = self.lblDisplay.text else {
            return
        }
        self.lblDisplay.text = result + value
    }
    
}

